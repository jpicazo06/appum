package com.example.umdashboard

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.MetaKeyKeyListener


class splash : AppCompatActivity() {

    private val SPLASH_TIME_OUT : Long = 1000
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        Handler( Looper.getMainLooper() ).postDelayed(
            {

                   val i = Intent(
                       this ,
                       login::class.java
                   )
                    startActivity( i )

            } , 1000
        )


    }

    override fun onResume() {
        super.onResume()
        Handler( Looper.getMainLooper() ).postDelayed(
            {
                val i = Intent(
                    this ,
                    login::class.java
                )
                startActivity( i )


            } , 1000
        )

    }
}