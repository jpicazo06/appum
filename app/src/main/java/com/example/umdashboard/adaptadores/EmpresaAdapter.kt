package com.example.umdashboard.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.umdashboard.R
import com.example.umdashboard.modelos.Empresas
import org.w3c.dom.Text

class EmpresaAdapter(var context: Context, items:ArrayList<Empresas>):BaseAdapter(){

    var items: ArrayList<Empresas>? = null
    init {
        this.items = items
    }


    override fun getCount(): Int {
        return items?.count()!!

    }
    override fun getItem(position: Int): Any {
        return items?.get(position)!!
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, convertView: View?, p2: ViewGroup?): View {
        var holder:ViewHolder? = null
        var vista:View? = convertView

        if ( vista == null ){
            vista = LayoutInflater.from( context ).inflate(
                    R.layout.empresas,
                    null
            )

            holder = ViewHolder(vista)

            vista.tag = holder
        }else {
            holder = vista.tag as? ViewHolder
        }

        val item = getItem(p0) as Empresas
        holder?.nombre?.text = item.nombre
        item.imagen?.let { holder?.imagen?.setImageResource(it) }

        return vista!!

    }


    }

    private class ViewHolder (vista: View){

        var nombre:TextView? = null
        var imagen : ImageView? = null

        init {
            nombre = vista.findViewById(R.id.textoEmpresa)
            imagen = vista.findViewById(R.id.imagen)
        }
    }


