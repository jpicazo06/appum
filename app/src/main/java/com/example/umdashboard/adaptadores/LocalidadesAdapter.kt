package com.example.umdashboard.adaptadores

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.example.umdashboard.R
import com.example.umdashboard.modelos.LocalidadesC


class LocalidadesAdapter ( var context : Context , items : ArrayList<LocalidadesC> ) : BaseAdapter() {

    var items : ArrayList<LocalidadesC>? = null
    init {
        this.items = items
    }


    override fun getCount(): Int {
        return items?.count()!!
    }

    override fun getItem(p0: Int): Any {
       return items?.get(p0)!!
    }

    override fun getItemId(p0: Int): Long {
       return p0.toLong()
    }

    override fun getView(p0: Int, convertView: View?, p2: ViewGroup?): View {
        var holder:ViewHolder? = null
        var vista:View? = convertView


        if ( vista == null ){
            vista = LayoutInflater.from( context ).inflate(
                R.layout.localidades_adapter,
                null
            )

            holder = ViewHolder(vista)

            vista.tag = holder
        }else {
            holder = vista.tag as? ViewHolder
        }

        val item = getItem(p0) as LocalidadesC

        holder?.nombre_Localidad?.text = item.nombre_Localidad
        holder?.volumen_Dia?.text = item.volumen_Dia.toString()
        holder?.promedio_Diario_Anualizado?.text = item.promedio_Diario_Anualizado.toString()
        holder?.acumulado_En_El_Mes?.text = item.acumulado_En_El_Mes.toString()
        holder?.acumulado_En_El_Anio?.text = item.acumulado_En_El_Anio.toString()
        holder?.objetivo_Anual?.text = item.objetivo_Anual.toString()
        holder?.porcentaje_de_Objetivo?.text = item.porcentaje_de_Objetivo.toString()


        return vista!!
    }


    private class ViewHolder( vista:View ){

        var  nombre_Localidad:TextView? = null
        var  volumen_Dia:TextView? = null
        var  promedio_Diario_Anualizado:TextView? = null
        var  acumulado_En_El_Mes:TextView? = null
        var  acumulado_En_El_Anio:TextView? = null
        var  objetivo_Anual:TextView? = null
        var  porcentaje_de_Objetivo:TextView? = null



        init {
            nombre_Localidad = vista.findViewById(R.id.tv_nombre_Localidad)
            volumen_Dia = vista.findViewById(R.id.et_volumen_Dia)
            promedio_Diario_Anualizado = vista.findViewById(R.id.et_promedio_Diario_Anualizado)
            acumulado_En_El_Mes = vista.findViewById(R.id.et_acumulado_En_El_Mes)
            acumulado_En_El_Anio = vista.findViewById(R.id.et_acumulado_En_El_Anio)
            objetivo_Anual = vista.findViewById(R.id.et_objetivo_Anual)
            porcentaje_de_Objetivo = vista.findViewById((R.id.et_porcentaje_de_Objetivo))
        }
    }


}


