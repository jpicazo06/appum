package com.example.umdashboard.ui.Anual

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AnualViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Anual"
    }
    val text: LiveData<String> = _text
}