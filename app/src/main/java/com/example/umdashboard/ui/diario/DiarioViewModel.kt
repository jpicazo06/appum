package com.example.umdashboard.ui.diario

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DiarioViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Diario"
    }
    val text: LiveData<String> = _text
}