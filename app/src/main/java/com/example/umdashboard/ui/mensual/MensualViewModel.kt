package com.example.umdashboard.ui.mensual

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MensualViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "Mensual"
    }
    val text: LiveData<String> = _text
}