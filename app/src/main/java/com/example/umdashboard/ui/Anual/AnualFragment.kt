package com.example.umdashboard.ui.Anual

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.umdashboard.R

class AnualFragment : Fragment() {

    private lateinit var anualViewModel: AnualViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        anualViewModel =
            ViewModelProvider(this).get(AnualViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_anual, container, false)
        val textView: TextView = root.findViewById(R.id.text_notifications)
        anualViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}