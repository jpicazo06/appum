package com.example.umdashboard.ui.diario

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.umdashboard.R

class DiarioFragment : Fragment() {

    private lateinit var diarioViewModel: DiarioViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        diarioViewModel =
            ViewModelProvider(this).get(DiarioViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_diario, container, false)
        val textView: TextView = root.findViewById(R.id.text_dashboard)
        diarioViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}