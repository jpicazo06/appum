package com.example.umdashboard.ui.mensual

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.umdashboard.R

class MensualFragment : Fragment() {

    private lateinit var mensualViewModel: MensualViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mensualViewModel =
            ViewModelProvider(this).get(MensualViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_mensual, container, false)
        val textView: TextView = root.findViewById(R.id.text_home)
        mensualViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}