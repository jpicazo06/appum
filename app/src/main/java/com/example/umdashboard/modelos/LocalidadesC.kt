package com.example.umdashboard.modelos

class LocalidadesC
    (
    nombre_Localidad : String ,
    volumen_Dia : Int,
    promedio_Diario_Anualizado : Int,
    acumulado_En_El_Mes : Int,
    acumulado_En_El_Anio : Double,
    objetivo_Anual : Double,
    porcentaje_de_Objetivo : Double
    )
{
    var nombre_Localidad : String = ""
    var volumen_Dia : Int = 0
    var promedio_Diario_Anualizado: Int = 0
    var acumulado_En_El_Mes : Int = 0
    var acumulado_En_El_Anio : Double = 0.0
    var objetivo_Anual : Double = 0.0
    var porcentaje_de_Objetivo : Double = 0.0


    init {
        this.nombre_Localidad = nombre_Localidad
        this.volumen_Dia = volumen_Dia
        this.promedio_Diario_Anualizado = promedio_Diario_Anualizado
        this.acumulado_En_El_Mes = acumulado_En_El_Mes
        this.acumulado_En_El_Anio = acumulado_En_El_Anio
        this.objetivo_Anual = objetivo_Anual
        this.porcentaje_de_Objetivo = porcentaje_de_Objetivo
    }


}