package com.example.umdashboard

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import com.example.umdashboard.adaptadores.EmpresaAdapter
import com.example.umdashboard.modelos.Empresas

class MainActivity : AppCompatActivity() {

        private lateinit var toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        var empresas : ArrayList<Empresas> = ArrayList()

        empresas.add(Empresas("AMSA", R.drawable.drop))
        empresas.add(Empresas("AMSA", R.drawable.drop))
        empresas.add(Empresas("AMSA", R.drawable.drop))
        empresas.add(Empresas("AMSA", R.drawable.drop))


        val lista = findViewById<ListView>(R.id.lista)

        val adaptador = EmpresaAdapter(
                this , empresas
        )

        lista.adapter = adaptador
        lista.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->

            val i = Intent(
                this ,
                Localidades::class.java
            )
            startActivity( i )
        }

    }
}