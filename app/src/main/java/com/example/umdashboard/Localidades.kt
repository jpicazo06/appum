package com.example.umdashboard

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import com.example.umdashboard.adaptadores.LocalidadesAdapter
import com.example.umdashboard.modelos.LocalidadesC

class Localidades : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_localidades)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        var Locs : ArrayList<LocalidadesC> = ArrayList()

        Locs.add(LocalidadesC(
            "Localidad 1",
            1000 ,
            10,
            2000,
            2200.00,
            100.44,
            222.22
        ))

        Locs.add(LocalidadesC(
            "Localidad 2",
            1000 ,
            10,
            2000,
            2200.00,
            222.22,
            100.44
        ))

        val listaLocalidades = findViewById<ListView>( R.id.listaLocalidades)

        val adaptador = LocalidadesAdapter(
            this ,
            Locs
        )

        listaLocalidades.adapter = adaptador


        listaLocalidades.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, position, id ->

            val i = Intent(
                this ,
                lecturas::class.java
            )
            startActivity( i )
        }




    }
}