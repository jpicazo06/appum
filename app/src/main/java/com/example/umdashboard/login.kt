package com.example.umdashboard

import android.content.Intent
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

class login : AppCompatActivity() {


    private val sBar  by lazy {
        val llLogin = findViewById<LinearLayout>(R.id.llLogin)
        Snackbar.make(
                llLogin,
                "No se pueden dejar campos vacios",
                Snackbar.LENGTH_SHORT
        )
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        val btnIngresar = findViewById<Button>(R.id.btnIngresar)
        btnIngresar.setOnClickListener {

               // Toast.makeText(this,"Listo", Toast.LENGTH_SHORT).show()


                val correo = findViewById<EditText>(R.id.correo)
                val contrasenaDos = findViewById<EditText>(R.id.contrasenaDos)

                if ( correo.text.toString()  == ""  || contrasenaDos.text.toString() == "" ) {
                    sBar.show()
                }else {
                    val i = Intent(
                            this ,
                            MainActivity::class.java
                    )
                    startActivity( i )
                }



               // startActivity( i )
            }
        }



    }
